This is a project to be used on at the MMO FED SIP Hackdays

## Clone Project ##

* Open up a Terminal Window

* **cd** to your chosen location to download the project to

* Clone the Project to your local machine: **git clone https://bitbucket.org/virginmediamultiscreen/mmo-fed-hackday.git**

## Install ##

* **cd mmo-fed-hackday** ( the project folder )

* **npm install**

* ( Once npm install has finished ) **node_modules/bower/bin/bower install**