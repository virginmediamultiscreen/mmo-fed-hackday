var VM = (function (vm) {
	"use strict";

	vm.calculator = (function () {
		var getIntById = function (id) {
				return parseInt(document.getElementById(id).value, 10);
			},
			add = function () {
				var sum = getIntById('x') + getIntById('y');
				document.getElementById('result').innerHTML = isNaN(sum) ? 0 : sum;
			},
			subtract = function () {
				var sum = getIntById('x') - getIntById('y');
				document.getElementById('result').innerHTML = isNaN(sum) ? 0 : sum;
			},
			init = function () {
				document.getElementById('add').addEventListener('click', add);
				document.getElementById('subtract').addEventListener('click', subtract);
			};

		return {
			init: init,
			add : add,
			subtract : subtract
		};

	}());

	return vm;

}(VM || {}));

if(typeof exports !== 'undefined') {
    exports.VM = VM;
}