'use strict';
fixture.base = 'src/apps/calculator/fixtures';
var assert = chai.assert;

describe('Calculator', function() {

	beforeEach(function() {

		fixture.load('calculator.html');
		
	   	VM.calculator.init() 
	});

	afterEach(function() {
    	fixture.cleanup();
  	});

	it('Add - should return the correct sum value of the specified 2 numbers added together', function () {
     	document.getElementById('x').value = '2';
   	 	document.getElementById('y').value = '2';
     	document.getElementById('add').click();
     	var result = document.getElementById('result').innerHTML
     	assert.equal( result, 4, '2+2 = 4')
	});
	  

	
    it('Subtract - should return the correct sum value of the specified 2 numbers subtracted', function () {
      document.getElementById('x').value = '2';
   	  document.getElementById('y').value = '2';
      document.getElementById('subtract').click();
      var result = document.getElementById('result').innerHTML
      assert.equal( result, 0, '2-2 = 0')
    });
	  

	it('should calculate zero for invalid x value', function() {
    	document.getElementById('x').value = 'hello';
    	document.getElementById('y').value = 2;
    	document.getElementById('add').click();
    	var result = document.getElementById('result').innerHTML
    	assert.equal( result, 0, '0 for invalid X value')
  	});

  	it('should calculate zero for invalid y value', function() {
    	document.getElementById('x').value = 2;
    	document.getElementById('y').value = 'hello';
    	document.getElementById('add').click();
    	var result = document.getElementById('result').innerHTML
    	assert.equal( result, 0, '0 for invalid Y value')
  	});
});